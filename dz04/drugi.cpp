#include <stdio.h>
#include <unistd.h>
#include <fcntl.h> //konstante za komunikaciju
#include <termios.h>
#include <stdint.h>

#define UART_PATH "/dev/serial0" //putanja do file-a koji predstavlja uart

void init_uart(int& fd){
    fd = open(UART_PATH, O_RDWR); //otvaram datoteku gdje mi je taj serial0,i reci cemo da tu datoteku mozemo i citati i pisati

    if(fd == -1) // ako se ne moze otvoriti
       perror("Error: Unable to open AURT\n");

    termios options();
    options.c_cflag = B9600 | CS8 | CREAD | CLOCAL;
    options.c_iflag = IGNPAR;
    options.c_oflag = 0;
    options.c_lflag = 0;

    tcflush(fd, TCIFLUSH);
    tcsetattr(fd, TCSANOW, &options);
}

/*
void send_byte(int& fd, uint16_t b){ //saljemo byte na Arduino
    if(fd == -1)
       perror("Error: Unable to open AURT\n");
    
    // b saljemo kao polje
    uint8_t buffer[1] = {b};

    
    if(write(fd, buffer, 1) < 0) // tako saljemo byte preko RPi-ja, if radimo u slucaju da nesto nije oke
       perror("Error: TX\n"); // greska pri slanju 
       
}
*/

void receive_byte(int& fd, uint16_t& b){ // primanje sa Arduina
    uint16_t buffer[1] = {}; //postavljam sve njegove argumente na 0

    if(fd != -1){  //ako je sve oke sa tim file-om
        read(fd, (void*)buffer, 1);
        b = buffer[0]; //to sto primimo spremamo u b
    } else{
        perror("Error: RX\n"); //greska pri primanju
    }
}


//unutar main cemo prihvacat podatke

int main(){

    int fd{};

    init_uart(fd);
    

    uint16_t r{0}; //ovo ce biti ono sto prihvacam(primim)
    while(true){ //sve dok ne dobijem maximalnu vrijednost za unsigned_int16 prihvacat cu
        usleep(50000);
        receive_byte(fd, r);
        printf("%u\n", r);
        if(r == 255)
           break;
    }

    close(fd);
    return 0;


}