/*
 * US_dz04_drugi_zad.c
 *
 * Created: 22.5.2020. 16:43:45
 * Author : martina
 */ 

#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "serial.h"


volatile uint8_t pin;
volatile uint16_t us_measurement = 0;  




void send_byte(uint16_t us_measurement){
	while(!(UCSR0A & (1 << UDRE0)));
	UDR0 = us_measurement;
	
}

void ir_measure(void){
	pin = (ADMUX & ( (1 << MUX3) | (1 << MUX2) | (1 << MUX1) | (1 << MUX0)));
	ADCSRA |= (1 << ADSC) | (1 << ADIE);
	return;
}

ISR(ADC_vect){
	if(pin == (ADMUX & ( (1 << MUX3) | (1 << MUX2) | (1 << MUX1) | (1 << MUX0)))){
		us_measurement = ADC;
		//uart_putint(us_measurement);
		} else {
		//uart_putchar('n');
	}
}

ISR(USART_TX_vect){//ako je slanje zavrseno
	UDR0 = us_measurement;  //samo cu to �to smo poslali spremiti u us_measurement

}

int main(void){
	uart_init();
	
	ADMUX |= (1 << REFS0) | (1 << MUX2);
	ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);
	ADCSRA |= (1 << ADEN);
	
	
	UBRR0 = 103;
	UCSR0C |= (1 << UCSZ01) | (1 << UCSZ00);
	UCSR0B |= (1 << TXEN0);     // | (RXEN0);
	
	UCSR0B |= (1 << TXCIE0);  //interrupt - je li SLANJE zavrsilo
	
	
	
	ir_measure();
	
	sei();
	
	while(1){
		ADCSRA |= (1 << ADSC);
		_delay_ms(100);
		
		
		send_byte(us_measurement);
		_delay_ms(100);
	}
	return 0;
}





