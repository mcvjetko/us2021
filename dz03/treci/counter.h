/*
 * counter.h
 *
 * Created: 7.5.2020. 12:06:54
 *  Author: martina
 */ 


#ifndef COUNTER_H_
#define COUNTER_H_

#include <avr/io.h>
#include <avr/interrupt.h>

void counter_init(){
	TCCR0B |= (1 << CS01) | (1 << CS00);  // preskejlali timer0
	
	TIMSK0 |= (1 << OCIE0A); //enable-ali interruptove za ovaj counter
	
	TCCR0B |= (1 << WGM01);
	
	TCCR0A |= (0 << COM0A1) | (1 << COM1A0);
	
	OCR0A = 250;
	sei();
	
}



#endif /* COUNTER_H_ */