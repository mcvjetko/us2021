/*
 * encoder.h
 *
 * Created: 7.5.2020. 12:08:42
 *  Author: martina
 */ 


#ifndef ENCODER_H_
#define ENCODER_H_

#include <avr/io.h>
#include <avr/interrupt.h>

volatile uint8_t A, B;
volatile int32_t ticks = 0;
volatile int32_t ticks_old = 0;

ISR(INT0_vect){
	A != B ? --ticks : ++ticks;
	A = PIND & (1 << PIND2) ? 1 : 0;
	
}

ISR(INT1_vect){
	B = PIND & (1 << PIND3) ? 1 : 0;
	A != B ? --ticks : ++ticks;
	
}

void encoder_init(){
	DDRD &= ~(1 << DDD2);
	DDRD &= ~(1 << DDD3);
	
	EICRA |= (1 << ISC00) | (1 << ISC10);
	EIMSK |= (1 << INT0) | (1 << INT1);
	sei();
	
	
}



#endif /* ENCODER_H_ */