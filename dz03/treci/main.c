/*
 * dz03_prvi.c
 *
 * Created: 7.5.2020. 11:51:49
 * Author : martina
 */ 

#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "serial.h"
#include "encoder.h"
#include "motor.h"
#include "counter.h"




volatile uint64_t ms = 0;

volatile int32_t P, I, D;
volatile int64_t err = 0;
volatile int64_t err_old = 0;
volatile int64_t err_acc = 0;  //za integral
volatile int64_t err_diff = 0; //za derivaciju

volatile int64_t gain = 0;
volatile int16_t speed = 0; //pomocna varijabla
volatile int64_t max = 1023;

volatile int16_t tps = 0;
volatile int16_t tps_m = 0;
volatile int16_t tps_2s = 0; //pomocna varijabla

/*
void reset_errors(){ //sve greske stavlja na 0
	err = 0;
	err_old = 0;
	err_acc = 0;
	err_diff = 0;
}
*/

volatile uint8_t sendToPlotter = 0;

ISR(TIMER0_COMPA_vect){ //interrupt service routine
	++ms;
	
	if(ms % 2000 == 0){
		if(tps_2s == 500){  //1000/2s = 500
			tps = (1500-1000) / 2;
			tps_2s = 750;  //1500/2s = 750 
			
		}
		else{
			tps = 500 - tps_2s;
			tps_2s = 500;
		}
	}
	
	
	
	if(ms % 20 == 0)
	    sendToPlotter = 1;
	
	
	if(ms % 5 == 0){ 
		tps_m = (ticks - ticks_old) * 1000 / 5; 
		
		
		err = tps - tps_m; 
		err_acc += err;
		err_diff = err - err_old;
		
		gain = P * err + I * err_acc + D * err_diff;
		
		gain /= 100;    
		
		if(gain > 0){ 
			gain += 430;
			speed = gain > max ? max : gain;
			OCR1A = speed;
			OCR1B = 0;
		}
		else{
			gain -= 430;
			gain *= -1;
			speed = gain > max ? max : gain;
			OCR1B = speed;
			OCR1A = 0;
		}
		
		err_old = err;
		ticks_old = ticks;
		
	}
}


int main()
{
	uart_init();
	encoder_init();
	motor_init();
	counter_init();
	
	//PID kontroler
	P = 15; 
	I = 6;
	D = 10;
	
	
    while (1)
    {
		
		//_delay_ms(1000);
		
		
		if(sendToPlotter){
			uart_putint(tps);
			uart_putstr("  ");
			
			uart_putint(tps_m);
			uart_putstr("\n");
			
		}
		
	}
	
	return 0;
}




