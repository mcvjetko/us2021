/*
 * serial.h
 *
 * Created: 7.5.2020. 11:54:29
 *  Author: martina
 */ 


#ifndef SERIAL_H_
#define SERIAL_H_

void uart_init();
void uart_putchar(char c);
void uart_putstr(char *data);
void uart_putint(int32_t n);







#endif /* SERIAL_H_ */