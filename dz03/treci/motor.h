/*
 * motor.h
 *
 * Created: 7.5.2020. 12:10:06
 *  Author: martina
 */ 


#ifndef MOTOR_H_
#define MOTOR_H_

#include <avr/io.h>

void motor_init(){
	DDRB |= (1 << DDB1) | (1 << DDB2);
	
	TCCR1B |= (1 << CS10);
	TCCR1A |= (1 << COM1A1) | (1 << COM1B1);
	
	TCCR1A |= (1 << WGM11) | (1 << WGM10);
	
}



#endif /* MOTOR_H_ */