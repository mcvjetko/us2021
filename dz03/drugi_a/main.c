/*
 * dz2_drugi.c
 *
 * Created: 22.4.2020. 18:30:39
 * Author : martina
 */ 

#ifndef F_CPU
#define F_CPU 16000000UL
#endif
#include<avr/io.h>
#include<util/delay.h>
#include<avr/interrupt.h>
#include "serial.h"



int main()
{
	uart_init();
	
	ADMUX |= (1 << MUX0) | (1 << MUX1) | (1 << REFS0) | (1 << REFS1);        // | (1 << MUX2) | (1 << MUX3); 
	
    
    while (1) 
    {
		if(ACSR & (1 << ACBG))       //  ACSR & (1 << ACO)  
		     uart_putstr("Ispod 1.1V\n");
		else uart_putstr("Iznad 1.1V\n");
		
		_delay_ms(500);
    }
	
	return 0;
}

