/*
 * serial.h
 *
 * Created: 22.4.2020. 18:31:07
 *  Author: martina
 */ 


#ifndef SERIAL_H_
#define SERIAL_H_

void uart_init();
void uart_putchar(char c);
void uart_putstr(char *data);
void uart_putint(uint16_t n);




#endif /* SERIAL_H_ */