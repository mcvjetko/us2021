/*
 * prvi_dz2.c
 *
 * Created: 23.4.2020. 14:11:27
 * Author : martina
 */ 

#ifndef F_CPU
#define F_CPU 16000000UL
#endif
#include<avr/io.h>
#include<util/delay.h>
#include<avr/interrupt.h>
#include "serial2.h"

volatile uint8_t pin;
volatile uint16_t us_measurement;

 void ir_measure(){
	 
	 ADCSRA |= (1 << ADSC); 
	 
	 _delay_ms(1000);
	 
	 pin = ADCH;
 }



ISR(ADC_vect){
	if(ADCH & (1 << pin)){
		us_measurement = pin;
		uart_putint(us_measurement);
		} 
	
}

int main(){
	uart_init();
	
	ADMUX |= (1 << REFS0) | (1 << MUX2) | (1 << ADLAR);
	ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (ADPS0);  
	ADCSRA |= (1 << ADEN);  
	
	
	ADCSRA |= (1 << ADSC) | (1 << ADIE); 
	
	sei();
	

	while(1){
		
		ir_measure();
			
	}
	
	return 0;

}



