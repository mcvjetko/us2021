/*
 * serial2.h
 *
 * Created: 23.4.2020. 14:19:23
 *  Author: martina
 */ 


#ifndef SERIAL2_H_
#define SERIAL2_H_

void uart_init();
void uart_putchar(char c);
void uart_putstr(char *data);
void uart_putint(uint16_t n);

#endif /* SERIAL2_H_ */