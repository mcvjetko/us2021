/*
 * serial4.h
 *
 * Created: 24.4.2020. 18:47:13
 *  Author: martina
 */ 


#ifndef SERIAL4_H_
#define SERIAL4_H_

void uart_init();
void uart_putchar(char c);
void uart_putstr(char *data);
void uart_putint(uint16_t n);




#endif /* SERIAL4_H_ */