/*
 * serial4.c
 *
 * Created: 24.4.2020. 18:46:43
 *  Author: martina
 */ 

#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#ifndef BAUD
#define BAUD 9600
#endif

#include<stdio.h>
#include<avr/io.h>
#include<avr/sfr_defs.h>
#include<stdlib.h>
#include<util/setbaud.h>
#include "serial4.h"

void uart_init(){
	UBRR0H = UBRRH_VALUE;
	UBRR0L = UBRRL_VALUE;
	UCSR0C = _BV(UCSZ01) | _BV(UCSZ00);
	UCSR0B = _BV(TXEN0);
}

void uart_putchar(char c){
	loop_until_bit_is_set(UCSR0A, UDRE0);
	UDR0 = c;
}

void uart_putstr(char *data){
	while(*data)
	   uart_putchar(*data++);
}

void uart_putint(uint16_t n){
	char str[10];
	itoa(n, str, 10);
	uart_putstr(str);
	uart_putchar("\n");
}