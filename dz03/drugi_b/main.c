/*
 * dz2_drugi_b.c
 *
 * Created: 24.4.2020. 18:46:10
 * Author : martina
 */ 

#ifndef F_CPU
#define F_CPU 16000000UL
#endif
#include<avr/io.h>
#include<util/delay.h>
#include<avr/interrupt.h>

#include "serial4.h"

ISR(INT0_vect){
	PORTD ^= (1 << PORTD4);
}


void beep(){
	PORTD |= (1 << PORTD4);
	_delay_ms(100);
	PORTD &= ~(1 << PORTD4);
	_delay_ms(100);
}


ISR(ANALOG_COMP_vect){
	if(ACSR & (1 << ACO)){
		
		beep();
	
		//_delay_ms(100);
		
	}
	
}

int main()
{
	uart_init();
	
	DDRD |= (1 << DDD4);
	
	sei();
	
	EICRA |= (1 << ISC00);
	EICRA |= (1 << ISC01);
	EIMSK |= (1 << INT0);
	
	PCICR |= (1 << PCIE2);
	PCMSK2 |= (1 << PCINT20);
	
    
    while (1) 
    {
    }
	
	return 0;
}

